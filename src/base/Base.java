package base;

import org.junit.Before;
import org.junit.Test;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

import systemTest.LoginSystemTesting;

public class Base {

	private WebDriver driver;
	
	public Base(WebDriver driver){
		this.driver=driver;
	}
	
	public void loadObject(String param,int time) {
		if (param != null) {
			System.out.println(param);
			WebElement webDriverWait = (WebElement) new WebDriverWait(driver, time)
					.until(ExpectedConditions.visibilityOfElementLocated(By.className(param)));
			//System.out.println(webDriverWait);
		}
	}
	
}

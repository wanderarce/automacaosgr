package systemTest;

import java.io.File;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.openqa.selenium.By;
import org.openqa.selenium.OutputType;
import org.openqa.selenium.TakesScreenshot;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.firefox.MarionetteDriver;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

import com.github.javafaker.Faker;
import com.sun.jna.platform.FileUtils;

import pages.PageObjects;
import pages.ProtocolRechargePage;
import base.Cpf;

public class ProtocolRechargeSystemTesting {

	private WebDriver driver;
	public WebElement telefoneSolicitante;
	public WebElement solicitante;
	
	Faker faker= new Faker();
	Cpf cpf = new Cpf();

	@Before
	public void init() {
		
		System.setProperty("webdriver.gecko.driver", "C:\\Program Files (x86)\\Mozilla Firefox\\geckodriver.exe");
		driver = new MarionetteDriver();
		driver.get("https://sgr-sit.redetendencia.com.br/sgr/paginas/login/login.html#/");
		PageObjects pageObjects = new PageObjects(driver);
		pageObjects.inputById("inputLogin", "consulta.serasa");
		pageObjects.inputById("inputSenha", "1010");
		pageObjects.submitForm("btn-primary");

	}
	
	@Test
	public void deveValidarCamposEmBranco(){
		try {
			PageObjects pageObjects = new PageObjects(driver);
			WebElement elem = new WebDriverWait(driver, 20)
					.until(ExpectedConditions.visibilityOfElementLocated(By
							.className("caret")));
			pageObjects.findLinkText("Atendimento");
			pageObjects.findLinkText("Credenciamento");
			
			ProtocolRechargePage protocolRechargePage = new ProtocolRechargePage(driver);
	
			protocolRechargePage.clicarEmIniciar();
			protocolRechargePage.getMessagesValidation("O campo Telefone solicitante é obrigatório");
			protocolRechargePage.getMessagesValidation("O campo Solicitante é obrigatório");
			
			
		} catch (Exception e) {
			e.getStackTrace();
			e.printStackTrace();
		}
		
	}
	
	//Regra: os documentos sao validados conforme a regra de geraçao da propria receita com base em calculos para definicao dos digitos verificadores 
	@Test
	public void deveValidarCPF(){
		try {
			PageObjects pageObjects = new PageObjects(driver);
			WebElement elem = new WebDriverWait(driver, 20)
					.until(ExpectedConditions.visibilityOfElementLocated(By
							.className("caret")));
			pageObjects.findLinkText("Atendimento");
			pageObjects.findLinkText("Credenciamento");
			
			ProtocolRechargePage protocolRechargePage = new ProtocolRechargePage(driver);
			protocolRechargePage.preencherSolicitacao("6712345678", "automacao");
			protocolRechargePage.selecionarNegocioRecarga();
			protocolRechargePage.selecionarTipoCredenciamento("Pequeno Varejo");
			protocolRechargePage.preencheDocumento("12345678991");
			protocolRechargePage.getMessagesValidation("O valor informado não corresponde ao CPF ou CNPJ válidos!");
			
			protocolRechargePage.preencheCep("79081370");
			protocolRechargePage.clicarEmIniciar();
			protocolRechargePage.selecionarEmpresa("REDE FLEX - MT");
			protocolRechargePage.clicarEmIniciar();
			
		} catch (Exception e) {
			//File source = ((TakesScreenshot)driver).getScreenshotAs(OutputType.FILE);
	        e.printStackTrace();  
		}
	}
	
	//Regra: Não deve permitir a abertura para cliente maior de oitenta
	@Test
	public void deveValidarMaiordeOitenta(){
		try {
			//seleciona o menu conforme passado nos parametros abaixo
			PageObjects pageObjects = new PageObjects(driver);
			WebElement elem = new WebDriverWait(driver, 20)
					.until(ExpectedConditions.visibilityOfElementLocated(By
							.className("caret")));
			pageObjects.findLinkText("Atendimento");
			pageObjects.findLinkText("Credenciamento");
			
			//preenche os dados basicos para a abertura de uma solicitacao
			ProtocolRechargePage protocolRechargePage = new ProtocolRechargePage(driver);
			protocolRechargePage.preencherSolicitacao("6712345678", "automacao");
			protocolRechargePage.selecionarNegocioRecarga();
			protocolRechargePage.selecionarTipoCredenciamento("Pequeno Varejo");
			protocolRechargePage.preencheDocumento("928.422.577-90");
			protocolRechargePage.getMessagesValidation("Solicitante com idade maior que 80 anos!");
			
			protocolRechargePage.preencheCep("79081370");
			protocolRechargePage.clicarEmIniciar();
			protocolRechargePage.selecionarEmpresa("REDE FLEX - MT");
			protocolRechargePage.clicarEmIniciar();
			
		} catch (Exception e) {
			// TODO: handle exception
			e.printStackTrace();
			e.getMessage();
		}
		
		
	}
	
	//Deve chegar até a fase Aguradando Consultor visto que todos os dados obrigatorios estão sendo preenchidos;
	@Test
	public void novoRecarga() throws InterruptedException {
		try {
			PageObjects pageObjects = new PageObjects(driver);
			WebElement elem = new WebDriverWait(driver, 20)
					.until(ExpectedConditions.visibilityOfElementLocated(By
							.className("caret")));
			pageObjects.findLinkText("Atendimento");
			pageObjects.findLinkText("Credenciamento");
			
			// preecnhimento de dados para solicitacao protocolo de Recarga
			ProtocolRechargePage protocolRechargePage = new ProtocolRechargePage(driver);
			protocolRechargePage.preencherSolicitacao("6712345678", faker.name().fullName());
			protocolRechargePage.selecionarNegocioRecarga();
			protocolRechargePage.selecionarTipoCredenciamento("Pequeno Varejo");
			protocolRechargePage.preencheDocumento(cpf.geraCPFFinal());
			protocolRechargePage.preencheCep("79081370");
			protocolRechargePage.clicarEmIniciar();
			protocolRechargePage.selecionarEmpresa("REDE FLEX - MT");
			protocolRechargePage.clicarEmIniciar();
			//solicitação inicial preenchida

			protocolRechargePage.prosseguirConsultaSerasa();
			
			//preenhcimento de dados do Estabelecimento
			protocolRechargePage.preencheDocumentosEstabelecimento("123456", "SSP", "MS");
			protocolRechargePage.alterarEmiteNotafiscal();
			protocolRechargePage.selecionaRamoAtividade("Centro Comericiais / Feiras / Eventos");
			protocolRechargePage.selecionaTipoCliente("Eletrônico");
			protocolRechargePage.preencheNumeroEndereco("133");
			
			//preenchimento de contato do Estabelecimento/Cliente
			protocolRechargePage.adicionaContato("contato automatizado");
			protocolRechargePage.salvarDadosGrid();
			protocolRechargePage.salvarItensContato("Celular", "67123456789");
			
			//preenchimento do Operador do Estabelecimento
			protocolRechargePage.adicionarOperador();
			protocolRechargePage.salvarDadosOperador("teste");
//			
//			//avancando para Analise de Credenciamento
			protocolRechargePage.clicarEmCredenciar();
			protocolRechargePage.prosseguirConsultaSerasa();
//			
			protocolRechargePage.voltarAPaginaAnterior();
//			
//			///Avancando para análise de Credito
			protocolRechargePage.prosseguirConsultaSerasa();
			protocolRechargePage.clicarEmCredenciar();
			protocolRechargePage.prosseguirConsultaSerasa();
			protocolRechargePage.prosseguirConsultaSerasa();
			
			//preenchendo Area/ Consultor
			protocolRechargePage.preenhcerDadosAreaConsultor("AREA 115 / CARLOS MOTTA JUNIOR");
			protocolRechargePage.preencherPeriodicidade("Sem visita");
		
			//inserindo Arquivo(s)
			protocolRechargePage.inserirAnexos("C:\\Users\\wander.arce\\Pictures\\","aba desabilitada.PNG");
			protocolRechargePage.clicarEmCredenciar();
			protocolRechargePage.prosseguirConsultaSerasa();
			
			protocolRechargePage.voltarAPaginaAnterior();
			
			protocolRechargePage.prosseguirConsultaSerasa();
			protocolRechargePage.clicarEmCredenciar();
			protocolRechargePage.prosseguirConsultaSerasa();
			protocolRechargePage.clicarEmCredenciar();
			protocolRechargePage.prosseguirConsultaSerasa();
			protocolRechargePage.voltarAPaginaAnterior();
			protocolRechargePage.prosseguirConsultaSerasa();
			
			protocolRechargePage.verificarFase();
		} catch (Exception e) {
			File source = ((TakesScreenshot)driver).getScreenshotAs(OutputType.FILE);
			e.printStackTrace();  
		}		
		
	}
	

	//Aguarda o periodo de ate 20 seg  para localizar o elemento
	public String delayTest(String param) {
		WebElement elem = new WebDriverWait(driver, 20)
				.until(ExpectedConditions.visibilityOfElementLocated(By
						.name(param)));
		if (!elem.equals(param))
			return "not found";
		return param;

	}
}

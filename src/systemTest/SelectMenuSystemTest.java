package systemTest;

import java.util.ArrayList;
import java.util.List;

import org.junit.Test;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

import base.Base;

public class SelectMenuSystemTest {

	private WebDriver driver;
	public WebElement menuPrincipal;
	public WebElement subMenu;
	
	public SelectMenuSystemTest() {
		super();
	}
	

	@Test
	public void clicarEmAtendimento()  {
		WebElement elem= new WebDriverWait(driver, 10).until(ExpectedConditions.visibilityOfElementLocated(By.id("btn-success")));
	    System.out.println(elem);
		menuPrincipal = driver.findElement(By.linkText("Atendimento"));
	    menuPrincipal.click();
		clicarEmCredenciamento();
	}

	@Test
	public void clicarEmCredenciamento() {
		subMenu = driver.findElement(By.linkText("Credenciamento"));
		subMenu.click();
		
	}
}
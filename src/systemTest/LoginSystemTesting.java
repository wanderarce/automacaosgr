package systemTest;

import java.util.ArrayList;
import org.junit.Before;
import org.junit.Test;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.firefox.FirefoxDriver;

import pages.LoginPage;
import pages.PageObjects;

public class LoginSystemTesting {

	private WebDriver driver;
	
	@Before
	public void openUrl() {
		driver = new FirefoxDriver();
		driver.get("https://sgr-sit.redetendencia.com.br/sgr/paginas/login/login.html#/");
	}
	
//	@Test
//	public void loginESenhaNulos(){
//		PageObjects pageObjects=new PageObjects(driver);
//		pageObjects.findId("inputLogin", "");
//		pageObjects.findId("inputSenha", "");
//		pageObjects.submitForm("btn-primary");
//		pageObjects.exitTest();
//	}
//	
//	@Test
//	public void loginIncorretos(){
//		PageObjects pageObjects=new PageObjects(driver);
//		pageObjects.findId("inputLogin", "usuario.automaca");
//		pageObjects.findId("inputSenha", "automacao2016");
//		pageObjects.submitForm("btn-primary");
//		pageObjects.exitTest();
//	}
//	
//
//	@Test
//	public void senhaIncorreta(){
//		PageObjects pageObjects=new PageObjects(driver);
//		pageObjects.findId("inputLogin", "usuario.automacao");
//		pageObjects.findId("inputSenha", "automacao2010");
//		pageObjects.submitForm("btn-primary");
//		pageObjects.exitTest();
//	}
//	
	@Test
	public void loginComDadosCorretos(){
		PageObjects pageObjects=new PageObjects(driver);
		pageObjects.findId("inputLogin", "consulta.serasa");
		pageObjects.findId("inputSenha", "1010");
		pageObjects.submitForm("btn-primary");
		//pageObjects.exitTest();
	}

}

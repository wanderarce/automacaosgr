package systemTest;

import java.awt.AWTException;

import org.junit.Before;
import org.junit.Test;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.firefox.MarionetteDriver;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

import pages.PageObjects;
import pages.ProtocolAcquiringPage;
import base.Cpf;

public class ProtocolAcquiringSystemTesting {

	static final String URL="https://sgr-sit.redetendencia.com.br/sgr/paginas/login/login.html#/";
	static final String PATH="C:\\Users\\wander.arce\\Pictures\\aba desabilitada.PNG";
	private WebDriver driver;
	public WebElement telefoneSolicitante;
	public WebElement solicitante;

	Cpf cpf = new Cpf();

	@Before
	public void init() {
		System.setProperty("webdriver.gecko.driver", "C:\\Program Files (x86)\\Mozilla Firefox\\geckodriver.exe");
		//System.setProperty("webdriver.gecko.driver", "/usr/lib/firefox/geckodriver");
		driver = new MarionetteDriver();
		driver.get(URL);
		PageObjects pageObjects = new PageObjects(driver);
		pageObjects.findId("inputLogin", "consulta.serasa");
		pageObjects.findId("inputSenha", "1010");
		pageObjects.submitForm("btn-primary");

	}

	@Test
	public void novoAdquirencia() throws InterruptedException, AWTException {

		//selecao de menu para abertura de protocolo Recarga
		PageObjects pageObjects = new PageObjects(driver);
		WebElement elem = new WebDriverWait(driver, 20)
				.until(ExpectedConditions.visibilityOfElementLocated(By
						.className("caret")));
		pageObjects.findLinkText("Atendimento");
		pageObjects.findLinkText("Credenciamento");

		// preecnhimento de dados para solicita��o do protocolo de Recarga
		ProtocolAcquiringPage protocolAcquiringPage = new ProtocolAcquiringPage(driver);
		protocolAcquiringPage.preencherSolicitacao("6712345678", "automacao");
		protocolAcquiringPage.selecionarNegocioAdquirencia();

		protocolAcquiringPage.preencheDocumento(cpf.geraCPFFinal());
		protocolAcquiringPage.preencheCep("79081370");
		protocolAcquiringPage.clicarEmIniciar();
		protocolAcquiringPage.selecionarEmpresa("REDE FLEX - MT");
		protocolAcquiringPage.clicarEmIniciar();

		protocolAcquiringPage.updateFase();
		
		protocolAcquiringPage.clicarEmIniciarFase();
		protocolAcquiringPage.confirmarAvancarFase();
		protocolAcquiringPage.inserirAnexos(PATH);

		protocolAcquiringPage.devePreencherDocumentosEstabelecimento(
				"123456","SSP", "MS","Razao Social Automatizada", "Nome Fantasia Automatizado",
				"10102010","67123456789","email@email.com"
				);
		
		protocolAcquiringPage.selecionaRamoAtividade("Centro Comericiais / Feiras / Eventos");
		protocolAcquiringPage.deveAlterarEmisteNotaFiscal();
		protocolAcquiringPage.clicarNaGrid("lkFuncionamento");

		protocolAcquiringPage.deveCadastrarHorarioDeFuncionamento("Segunda Feira","0000","2359");
		protocolAcquiringPage.devePreencherEnderecodoEstabelecimento("200");
		
		//preenchimento de contato e itens Contato
		protocolAcquiringPage.clicarNaGrid("lkContato");
		protocolAcquiringPage.adicionaContato("contato automatizado");
		//protocolAcquiringPage.salvarItensContato("Celular", "67123456789");
		protocolAcquiringPage.salvarItensContato("Email NFE", "email@nfe.com");

		protocolAcquiringPage.clicarNaGrid("lkOperador");

		protocolAcquiringPage.clicarEmAdicionarOperador();
		protocolAcquiringPage.deveCadastrarOperador("nome",true);

		//preenchimento de condições comerciais
		protocolAcquiringPage.clicarNaGrid("lkCondicoesComerciais");
		
		protocolAcquiringPage.deveCadastrarCondicoesComerciais("Elavon","Caça e serviços relacionados");
		protocolAcquiringPage.avancarFase();
		
		//preenchimento de Equipamento(s) solicitado(s) para o Cliente
		protocolAcquiringPage.clicarNaGrid("lkEquipamento");
		protocolAcquiringPage.clicarNoAdicionarEquipamento();
		protocolAcquiringPage.deveCadastrarUmEquipamento("1", "Dia 05", "Nenhum");
		
		//preenchimento de Detalhes de credito
		protocolAcquiringPage.clicarNaGrid("lkDetalheCredito");
		protocolAcquiringPage.deveCadastrarDetalheCredito("Imediata", "23132", "654");
		
		//preenchimento de Domicilio Bancario
		protocolAcquiringPage.clicarNaGrid("lkDomicilioBancario");
		protocolAcquiringPage.deveCadastrarDomiciilioBancario("Banco do Brasil", "123", "456");

		protocolAcquiringPage.deveAdicionarTodasAsBandeiras(new String[]{"Dinners Club","Visa","Master Card", "Ticket","Discover", "VR Benefícios" });
		
		protocolAcquiringPage.avancarFase();
		protocolAcquiringPage.clicarEmIniciarFase();
		protocolAcquiringPage.confirmarAvancarFase();
		
		//preenchendo area/consultor/periodicidade
		protocolAcquiringPage.preenhcerDadosAreaConsultor("AREA 115 / CARLOS MOTTA JUNIOR");
		protocolAcquiringPage.preencherPeriodicidadeSemData("Sem visita");
		
		//avancando para Enviar para Adquirente
		protocolAcquiringPage.avancarFase();
		protocolAcquiringPage.clicarEmIniciarFase();
		protocolAcquiringPage.confirmarAvancarFase();
		
		//avancando para Aguardando Adquirente
		protocolAcquiringPage.avancarFase();
		protocolAcquiringPage.clicarEmIniciarFase();
		protocolAcquiringPage.confirmarAvancarFase();
		 
		//preenchendo aprovacao pela adquirente
		protocolAcquiringPage.clicarEmAprovadoPelaAdquirente();
		protocolAcquiringPage.clicarEmResponsavelEmpresa();
		protocolAcquiringPage.preencherMerchant("13115161651");
		
		//preenchendo equipamento adquirente
		protocolAcquiringPage.clicarNaGrid("lkEquipamentoAdquirente");
		protocolAcquiringPage.deveCadastrarEquipamentoAdquirente("131656","161566");
	
		protocolAcquiringPage.urlAtual();
		//avancando para Criar no SGV
		protocolAcquiringPage.avancarFase();
				
		//protocolAcquiringPage.voltarAPaginaAnterior();
		protocolAcquiringPage.verificarFase();
	}



	public String delayTest(String param) {
		WebElement elem = new WebDriverWait(driver, 20)
				.until(ExpectedConditions.visibilityOfElementLocated(By
						.name(param)));
		if (!elem.equals(param))
			return "not found";
		return param;

	}
	
	
}

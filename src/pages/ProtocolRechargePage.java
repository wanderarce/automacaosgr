package pages;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

import junit.framework.Assert;

import org.junit.Test;
import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptException;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.Select;
import org.openqa.selenium.support.ui.WebDriverWait;

public class ProtocolRechargePage {

	public WebDriver driver;
	public WebElement telefoneSolicitante, nomeSolicitante, ckAdquirencia,
			ckRecarga, typeAccreditation, document, postalCode, business,
			start, message;
	public List<WebElement> messages,
			itensContact = new ArrayList<WebElement>();
	String menorIdade = "O titular do documento informado � menor de idade! Favor perguntar para o solicitante se ele � menor emancipado?! Clique em Sim para continuar ou clique em N�o para encerrar o credenciamento!";
	String maiorDeOitenta = "Solicitante com idade maior que 80 anos";

	public ProtocolRechargePage(WebDriver driver) {
		this.driver = driver;
	}

	@Test
	public void preencherSolicitacao(String telefone, String solicitante)
			throws InterruptedException {
		Thread.sleep(1000);
		telefoneSolicitante = driver.findElement(By.id("txtTelefone"));
		nomeSolicitante = driver.findElement(By.id("txtNome"));
		telefoneSolicitante.sendKeys(telefone);
		nomeSolicitante.sendKeys(solicitante);

	}

	public void selecionarNegocioAdquirencia() {

		ckAdquirencia = driver.findElement(By.id("ckAdquirencia"));
		ckAdquirencia.click();
	}

	public void selecionarNegocioRecarga() {
		ckRecarga = driver.findElement(By.id("ckRecarga"));
		ckRecarga.click();
	}

	public void selecionarTipoCredenciamento(String tipoCredenciamento)
			throws InterruptedException {
		Thread.sleep(1000);
		Select select = new Select(driver.findElement(By
				.id("cmbTipoCredenciamento")));
		select.selectByVisibleText("Pequeno varejo");
	}

	public void selecionarEmpresa(String empresa) {
		Select select = new Select(driver.findElement(By.id("cmbEmpresa")));
		select.selectByVisibleText(empresa);
	}

	public void preencheDocumento(String cpfCnpj) throws InterruptedException {
		document = driver.findElement(By.id("txtCpfCnpj"));
		document.sendKeys(cpfCnpj);
		Thread.sleep(1000);
	}

	public void preencheCep(String cep) throws InterruptedException {
		Thread.sleep(2000);
		postalCode = driver.findElement(By.id("txtCep"));
		postalCode.sendKeys(cep);
	}

	public void clicarEmIniciar() throws InterruptedException {
		Thread.sleep(2000);
		start = new WebDriverWait(driver, 120).until(ExpectedConditions
				.visibilityOfElementLocated(By.className("btn-success")));
		start.click();
	}

	public void prosseguirConsultaSerasa() throws InterruptedException {
		start = new WebDriverWait(driver, 120).until(ExpectedConditions
				.visibilityOfElementLocated(By.className("modal-footer")));
		System.out.println(start.toString());
		messageBody();
		Thread.sleep(3000);
	}

	public void messageBody() {
		message = new WebDriverWait(driver, 120).until(ExpectedConditions
				.visibilityOfElementLocated(By.className("modal-body")));
		if (message.getText().equals(menorIdade)) {
			menorDeIdade();
		}
		WebElement confirm = start.findElement(By.className("btn-success"));
		System.out.println(confirm.getText());
		confirm.click();
	}

	public void menorDeIdade() {
		start = new WebDriverWait(driver, 120).until(ExpectedConditions
				.visibilityOfElementLocated(By.className("modal-footer")));

		WebElement confirm = start.findElement(By.className("btn-success"));
		System.out.println(confirm.getText());
		confirm.click();
	}

	public void maiorDeOitenta() {
		WebElement confirm = start.findElement(By.className("btn-success"));
		System.out.println(confirm.getText());
		confirm.click();
	}

	public void preencheDocumentosEstabelecimento(String rg,
			String orgaoEmissor, String uf) throws InterruptedException {
		Thread.sleep(4000);
		driver.findElement(By.id("txtRG")).sendKeys(rg);
		driver.findElement(By.id("txtOrgaoEmissor")).sendKeys(orgaoEmissor);
		Select select = new Select(driver.findElement(By.id("cmbUfEmissao")));
		select.selectByVisibleText(uf);
	}

	public void selecionaRamoAtividade(String ramoAtividade) {
		Select select = new Select(
				driver.findElement(By.id("cmbRamoAtividade")));
		select.selectByVisibleText(ramoAtividade);
	}

	public void selecionaTipoCliente(String tipoCliente) {
		Select select = new Select(driver.findElement(By.id("cmbTipoCliente")));
		select.selectByVisibleText(tipoCliente);
	}

	public void preencheNumeroEndereco(String num) {
		driver.findElement(By.id("txtNumero")).sendKeys(num);
	}

	public void preencheComplementoEndereco(String complemento) {
		driver.findElement(By.id("txtNumero")).sendKeys(complemento);
	}

	public void adicionaSocio(String nomeSocio) {
		driver.findElement(By.className("txtNomeSocio")).sendKeys(nomeSocio);
		;
	}

	public void adicionaContato(String nomeContato) throws InterruptedException {
		WebElement divContato = driver.findElement(By.id("Contato"));
		divContato.findElement(By.className("glyphicon-plus")).click();
		// WebElement form=driver.findElement(By.name("formContato"));
		Thread.sleep(4000);
		divContato.findElement(By.id("txtNomeContato")).click();
		divContato.findElement(By.id("txtNomeContato")).sendKeys(nomeContato);
	}

	public void salvarItensContato(String tipoContato, String contato)
			throws InterruptedException {
		Thread.sleep(4000);
		PageObjects pageObjects = new PageObjects(driver);
		pageObjects.selectOption("cmbTipoContato", tipoContato);
		WebElement elem = driver.findElement(By.name("txtContato"));
		WebElement tipo = driver.findElement(By.name("cmbTipoContato"));
		System.out.println("contato" + elem.getText());
		pageObjects.inputByName("txtContato", contato);
		WebElement gridContato = driver.findElement(By.id("Contato"));
		tipo.click();
		gridContato.findElement(By.className("glyphicon-floppy-disk")).click();
	}

	

	public void salvarDadosGrid() throws InterruptedException {
		Thread.sleep(4000);
		driver.findElement(By.className("btn-success")).click();
	}

	public void adicionarOperador() throws InterruptedException {
		Thread.sleep(4000);
		WebElement op = driver.findElement(By.id("operadorForm"));
		op.findElement(By.className("btn-success")).click();
	}

	public void salvarDadosOperador(String nomeOperador)
			throws InterruptedException {
		driver.findElement(By.id("txtNomeOperador")).sendKeys(nomeOperador);
		driver.findElement(By.id("papelOperadorComponent")).click();
		Thread.sleep(2000);

		WebElement parent = driver.findElement(By
				.xpath("//span[@id='papelOperadorComponent']"));// div[@class='multiSelectOptions']/div[@class='multiSelectValues']/div[@class='multiSelectItem']/div[@class='multiSelect']/label[@class='multiSelect']"));
		List<WebElement> e = parent.findElements(By
				.cssSelector("div.multiSelect.acol"));// label.multiSelect"));
		for (WebElement p : e) {
			// System.out.println("Valor" +p.getText());

			if ((p.getText().contains("Terminal Administrador"))
					|| (p.getText().contains("Terminal Aceite"))) {

				System.out.println(p.getText());
				p.click();
			}

		}
		WebElement gridOperador = driver.findElement(By.id("Operador"));
		WebElement btnSalvar = gridOperador.findElement(By
				.className("btn-success"));
		btnSalvar.click();

	}

	public void clicarEmCredenciar() throws InterruptedException {
		Thread.sleep(3000);

		WebElement footer = driver.findElement(By.className("panel-footer"));
		WebElement btns = footer.findElement(By.className("btn-success"));
		btns.click();
	}

	public void alterarEmiteNotafiscal() {
		driver.findElement(By.name("rtNotaFistal")).click();

	}

	public void detalharDadosProtocolo(String protocolo)
			throws InterruptedException {
		Thread.sleep(3000);
		driver.findElement(By.name("inputProtocolo")).sendKeys(protocolo);
		driver.findElement(By.className("bypass")).click();
		Thread.sleep(3000);
		driver.findElement(By.className("glyphicon-new-window")).click();
		Thread.sleep(3000);
		driver.findElement(By.id("lblProtocolo")).click();

	}

	public void preenhcerDadosAreaConsultor(String area) {
		PageObjects pageObjects = new PageObjects(driver);
		pageObjects.selectOption("cmbArea", area);

	}

	public void preencherPeriodicidade(String periodicidade) {
		PageObjects pageObjects = new PageObjects(driver);
		pageObjects.selectOption("cmbPeriodicidade", periodicidade);

	}

	public void inserirAnexos(String path, String imagem)
			throws InterruptedException {
		driver.findElement(By.name("file")).sendKeys(path + imagem);
		Thread.sleep(5000);
	}

	public void voltarAPaginaAnterior() throws InterruptedException {
		Thread.sleep(10000);
		driver.navigate().back();
	}

	public void verificarFase() throws InterruptedException {
		Thread.sleep(3000);
		boolean fase = driver.getPageSource().contains("Aguardando Consultor");

		if (fase) {
			driver.quit();
		} else {
			String urlAtual= driver.getCurrentUrl();
			atualizarPagina(urlAtual);
			verificarFase();
		}

	}
	
	public void atualizarPagina(String url){
		driver.get(url);
	}
	
	//verificacao de mensagens no sistema
	public void messages() {
		message = (WebElement) driver.findElements(By.linkText(maiorDeOitenta));
	}
	
	public void getMessagesValidation(String message) {
		
		boolean messageCaptured=driver.getPageSource().contains(message);
		if (messageCaptured) {
			System.out.println("Não encontrado");
		}

		System.out.println( message);
		
	}
}

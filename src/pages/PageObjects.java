package pages;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.Select;
import org.openqa.selenium.support.ui.WebDriverWait;

import base.Base;

public class PageObjects {

	public WebDriver webDriver;

	public PageObjects(WebDriver webDriver) {
		this.webDriver = webDriver;

	}

	public void getAddress(String url) {
		webDriver.get(url);

	}

	public void findId(String id, String value) {
		loadObjetct(id);
		webDriver.findElement(By.id(id)).sendKeys(value);
	}

	public void findName(String name, String value) {
		loadObjetct(name);
		webDriver.findElement(By.name(name)).sendKeys(value);
	}

	public void findClassName(String className, String value) {
		loadObjetct(className);
		webDriver.findElement(By.cssSelector(className)).sendKeys(value);
	}

	public void selectItemClassName(String className) throws InterruptedException {
		Thread.sleep(2000);
		WebElement select= webDriver.findElement(By.className(className));
		System.out.println(select.getTagName());
		
	}
	
	public void findCssSelector(String cssSelector, String value) {
		loadObjetct(cssSelector);
		webDriver.findElement(By.cssSelector(cssSelector)).sendKeys(value);
	}

	public void findTagName(String tag, String value) {
		loadObjetct(tag);
		webDriver.findElement(By.tagName(tag)).sendKeys(value);
	}

	public void findPath(String xpath, String value) {
		
		webDriver.findElement(By.xpath(xpath)).sendKeys(value);
	}

	public void findLinkText(String link) throws InterruptedException {
		Thread.sleep(3000);
		webDriver.findElement(By.linkText(link)).click();
	}

	public void findPartialLink(String partialLink) {
		loadObjetct(partialLink);
		webDriver.findElement(By.linkText(partialLink)).click();
	}

	public void clickById(String id) {

		webDriver.findElement(By.id(id)).click();
	}
	
	public void submitForm(String btn) {
		webDriver.findElement(By.className(btn)).click();
		
	}

	public void selectOption(String param, String value) {
		loadObjetctByName(param);
		Select select = new Select(webDriver.findElement(By.name(param)));
		select.selectByVisibleText(value);
	}

	public void selectOptionByClassName(String className, String value) {
		
		Select select = new Select(webDriver.findElement(By.className(className)));
		select.selectByVisibleText(value);
	}
	
	public void exitTest() {
		webDriver.quit();
	}

	public void loadObjetct(String param) {
		if (param != null) {
			WebElement element = new WebDriverWait(webDriver, 120)
					.until(ExpectedConditions.visibilityOfElementLocated(By
							.id(param)));
			System.out.println(element);
		}else{
			WebElement element = new WebDriverWait(webDriver, 120)
			.until(ExpectedConditions.visibilityOfElementLocated(By
					.name(param)));
			System.out.println(element);
		}

	}

	public void loadObjetctByName(String param) {
		if (param != null) {
			WebElement element = new WebDriverWait(webDriver, 120)
			.until(ExpectedConditions.visibilityOfElementLocated(By
					.name(param)));
			System.out.println(element);
		}

	}
	
	public void loadButon(String param) {
		if (param != null) {
			WebElement element = new WebDriverWait(webDriver, 120)
			.until(ExpectedConditions.visibilityOfElementLocated(By
					.className(param)));
			element.findElement(By.className(param)).click();
		}

	}
	
	public void selectCheckBox(String param){
		if (param != null) {
			webDriver.findElement(By.className(param)).click();
			System.out.println(param);
		}
	}
	
	public void inputById(String id, String value) {
		loadObjetct(id);

		System.out.println("Parametro : " + value);
		webDriver.findElement(By.id(id)).sendKeys(value);
		System.out.println("Parametro : " + webDriver.findElement(By.id(id)).getText());
	}

	public void inputByName(String name, String value) {
		loadObjetct(name);
		webDriver.findElement(By.name(name)).sendKeys(value);
	}
}

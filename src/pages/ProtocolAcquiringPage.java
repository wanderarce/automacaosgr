package pages;

import java.awt.AWTException;
import java.awt.Robot;
import java.awt.Toolkit;
import java.awt.datatransfer.StringSelection;
import java.awt.event.KeyEvent;
import java.util.ArrayList;
import java.util.List;

import org.junit.Before;
import org.junit.Test;
import org.openqa.selenium.By;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.interactions.Action;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.Select;
import org.openqa.selenium.support.ui.WebDriverWait;

import static org.openqa.selenium.OutputType.*;
import base.Cpf;

public class ProtocolAcquiringPage {

	public WebDriver driver;
	public WebElement telefoneSolicitante, nomeSolicitante, ckAdquirencia,ckRecarga ,typeAccreditation, document, postalCode, business, start,message;
	public List<WebElement> messages,itensContact = new ArrayList<WebElement>();
	Action action;

	public ProtocolAcquiringPage(WebDriver driver) {
		this.driver = driver;
	}
	

	@Test
	public void preencherSolicitacao(String telefone, String solicitante) throws InterruptedException {
		Thread.sleep(1000);
		telefoneSolicitante=driver.findElement(By.id("txtTelefone"));
		nomeSolicitante=driver.findElement(By.id("txtNome"));
		telefoneSolicitante.sendKeys(telefone);
		nomeSolicitante.sendKeys(solicitante);

	}

	public void selecionarNegocioAdquirencia(){
		ckAdquirencia= driver.findElement(By.id("ckAdquirencia"));
		ckAdquirencia.click();
	}

	public void selecionarNegocioRecarga(){
		ckRecarga= driver.findElement(By.id("ckRecarga"));
		ckRecarga.click();
	}


	public void selecionarEmpresa(String empresa){
		Select select= new Select(driver.findElement(By.id("cmbEmpresa")));
		select.selectByVisibleText(empresa);
	}

	public void preencheDocumento(String cpfCnpj) throws InterruptedException{
		document= driver.findElement(By.id("txtCpfCnpj"));
		document.sendKeys(cpfCnpj);
		Thread.sleep(1000);
	}

	public void preencheCep(String cep) throws InterruptedException{
		Thread.sleep(2000);
		postalCode= driver.findElement(By.id("txtCep"));
		postalCode.sendKeys(cep);
	}

	public void clicarEmIniciar() throws InterruptedException {
		Thread.sleep(2000);
		start= new WebDriverWait(driver, 180)
		.until(ExpectedConditions.visibilityOfElementLocated(By
				.className("btn-success")));
//		if(start.getText().equals("Iniciar")){
//			start.click();
//		}
		start.click();
		
	}

	public void devePreencherDocumentosEstabelecimento(String rg,String orgaoEmissor,String uf, String razaoSocial, String nomeFantasia,String dataNascimentoConstituicao, CharSequence celularEnvioSenha, CharSequence emailRelacionamento) throws InterruptedException{
		Thread.sleep(4000);
		System.out.println(rg);
		driver.findElement(By.id("lkEstabelecimento"));
		driver.findElement(By.name("txtRG")).sendKeys(rg);
		driver.findElement(By.name("txtOrgaoEmissor")).sendKeys(orgaoEmissor);
		Select select= new Select(driver.findElement(By.name("cmbUfEmissao")));
		select.selectByVisibleText(uf);
		driver.findElement(By.name("txtRazaoSocial")).sendKeys(razaoSocial);
		driver.findElement(By.name("txtNomeFantasia")).sendKeys(nomeFantasia);
		Thread.sleep(2000);
		driver.findElement(By.name("dtConstituicao_")).click();
		driver.findElement(By.name("dtConstituicao_")).sendKeys(dataNascimentoConstituicao);
		Thread.sleep(3000);
		
		driver.findElement(By.name("txtCelularEnvioSenha")).sendKeys(celularEnvioSenha);
		driver.findElement(By.name("txtEmailRelacionamento")).sendKeys(emailRelacionamento);

	}

	public void deveCadastrarHorarioDeFuncionamento(String periodoFuncionamento, String horarioInicio, String horarioTermino) throws InterruptedException{
		Thread.sleep(2000);
		WebElement func=driver.findElement(By.id("Funcionamento"));
		func.findElement(By.className("btn-success")).click();
		Select select= new Select(driver.findElement(By.name("cmbFuncionamento")));
		select.selectByVisibleText(periodoFuncionamento);
		driver.findElement(By.name("txtInicio")).sendKeys(horarioInicio);
		driver.findElement(By.name("txtFim")).sendKeys(horarioTermino);
		WebElement btnAdcionarHorario=driver.findElement(By.className("glyphicon-floppy-disk"));

		btnAdcionarHorario.click();

	}

	public void devePreencherEnderecodoEstabelecimento(String numero){
		driver.findElement(By.name("txtNumero")).sendKeys(numero);
	}

	public void selecionarUf(String uf){
		Select select= new Select(driver.findElement(By.id("cmbUfEmissao")));
		select.selectByVisibleText(uf);
	}

	public void selecionaRamoAtividade(String ramoAtividade){
		Select select= new Select(driver.findElement(By.name("cmbRamoAtividade")));
		select.selectByVisibleText(ramoAtividade);
	}

	public void selecionaTipoCliente(String tipoCliente){
		Select select= new Select(driver.findElement(By.id("cmbTipoCliente")));
		select.selectByVisibleText(tipoCliente);
	}

	public void preencheNumeroEndereco(String num){
		driver.findElement(By.id("txtNumero")).sendKeys(num);
	}

	public void preencheComplementoEndereco(String complemento){
		driver.findElement(By.id("txtNumero")).sendKeys(complemento);
	}

	public void adicionaSocio(String nomeSocio){
		driver.findElement(By.className("txtNomeSocio")).sendKeys(nomeSocio);;
	}

	public void adicionaContato(String nomeContato) throws InterruptedException{
		Thread.sleep(2000);
		WebElement divContato= driver.findElement(By.id("Contato"));
		divContato.findElement(By.className("glyphicon-plus")).click();
		Thread.sleep(2000);
		divContato.findElement(By.name("txtNomeContato")).sendKeys(nomeContato);
		divContato.findElement(By.className("glyphicon-floppy-disk")).click();
	}

	public void salvarItensContato(String tipoContato, String contato) throws InterruptedException{
		Thread.sleep(2000);
		PageObjects pageObjects= new PageObjects(driver);
		pageObjects.selectOption("cmbTipoContato", tipoContato);
		WebElement elem=driver.findElement(By.name("txtContato"));
		System.out.println("contato" + elem.getText());
		pageObjects.inputByName("txtContato",contato);
		driver.findElement(By.name("swContatoPrincipal")).click();
		Thread.sleep(2000);

		WebElement divContato= driver.findElement(By.id("Contato"));
		//WebElement btnSuccess=driver.findElement(By.className("action-column"));

		divContato.findElement(By.xpath("//div[@class='table-responsive']/table/tbody/tr[2]/td[2]/expand-row/table/tbody/tr/td[1]/span[2]/button[1]")).click();
	}

	public void salvarDadosGrid() throws InterruptedException{
		Thread.sleep(4000);
		WebElement elem=driver.findElement(By.className("ng-binding"));
		elem.click();
	}

	public void clicarEmAdicionarOperador() throws InterruptedException{
		Thread.sleep(4000);
		WebElement elem=driver.findElement(By.id("Operador"));
		WebElement btnAdicionar= elem.findElement(By.className("btn-success"));;
//		System.out.println(btnAdicionar.getText());
//		if(btnAdicionar.getText().equals("Adicionar Operador")){
			btnAdicionar.click();
		//}
	}

	public void clicarEmIniciarFase()  throws InterruptedException{
		Thread.sleep(3000);
		new WebDriverWait(driver, 180).until(ExpectedConditions
				.visibilityOfElementLocated(By.className("glyphicon-flash")));
		new Actions(driver).moveToElement(driver.findElement(By.className("glyphicon-flash")));
	
		List<WebElement> btnIniciar= driver.findElements(By.className("btn-success"));
		for (WebElement btn : btnIniciar) {
			System.out.println(btn.getText());
			if(btn.getText().equals("Iniciar")){
				btn.click();
			}
		}
		
	}

	public void updateFase() throws InterruptedException{
		Thread.sleep(3000);

		WebElement element = new WebDriverWait(driver, 120)
		.until(ExpectedConditions.visibilityOfElementLocated(By
				.id("lblProtocolo")));
		System.out.println(element);

	}

	public void confirmarAvancarFase(){
		start= new WebDriverWait(driver, 120)
		.until(ExpectedConditions.visibilityOfElementLocated(By
				.className("modal-footer")));
		System.out.println(start.toString());
		start.findElement(By.className("btn-success")).click();


	}

	public void clicarNaGrid(String gridId) throws InterruptedException{
		Thread.sleep(3000);
		driver.findElement(By.id(gridId)).click();

	}

	public void btnAddGrid(String btn){
		WebElement btnAdicionar= driver.findElement(By.className(btn));
		if(btnAdicionar.getText().equals("Adicionar Contato")){
			btnAdicionar.click();
		}
	}

	public void clicarNoAdicionarEquipamento() throws InterruptedException{

		WebElement divEquipamento= driver.findElement(By.id("Equipamento"));
		Thread.sleep(2000);
		WebElement btnAdicionar= driver.findElement(By.cssSelector("i.glyphicon.glyphicon-plus"));
		System.out.println("Oi");
		btnAdicionar.click();

	}

	public void deveCadastrarUmEquipamento(CharSequence quantidadeTerminais, String diaVencimento, String isencaoInicial){
		driver.findElement(By.name("rdTecnologiaGroupundefined")).click();
		driver.findElement(By.id("rdTerminal_undefined_POS")).click();
		driver.findElement(By.name("txtNumeroTerminais")).sendKeys(quantidadeTerminais);
		PageObjects pageObjects= new PageObjects(driver);
		pageObjects.selectOption("cmbDataVencimento", diaVencimento);
		pageObjects.selectOption("cmbIsencaoInicial", isencaoInicial);
		WebElement gridEquipamento=driver.findElement(By.id("Equipamento"));
		gridEquipamento.findElement(By.className("glyphicon-floppy-disk")).click();

	}
	public void deveCadastrarDetalheCredito(String parzoMedia, String faturamentoAnualCartao, String ticketMedioCartao){
		PageObjects pageObjects=new PageObjects(driver);
		pageObjects.selectOption("cmbPrazoMedioEntrega", parzoMedia);
		pageObjects.inputByName("txtFaturamentoCartao", faturamentoAnualCartao);
		pageObjects.inputByName("txtTicketMedioCartao", ticketMedioCartao);

	}

	public void deveCadastrarDomiciilioBancario(String bancoAdquirente, String numeroAgencia, String numeroConta) throws InterruptedException{
		Thread.sleep(2000);
		WebElement gridDomicilioBancario=driver.findElement(By.id("domicilioBancarioContainer"));
		gridDomicilioBancario.findElement(By.className("glyphicon-plus")).click();

		WebElement inputBanco=driver.findElement(By.name("txtNameacBanco_undefined"));
		inputBanco.sendKeys(bancoAdquirente);

		PageObjects pageObjects= new PageObjects(driver);
		pageObjects.findLinkText("Banco do Brasil");

		WebElement inputAgencia=gridDomicilioBancario.findElement(By.name("txtNumeroAgencia"));
		inputAgencia.click();
		inputAgencia.sendKeys(numeroAgencia);

		gridDomicilioBancario.findElement(By.name("txtNumeroConta")).sendKeys(numeroConta);
		WebElement domicilio=driver.findElement(By.id("DomicilioBancario"));
		domicilio.findElement(By.className("glyphicon-floppy-disk")).click();

	}

	public void deveAdicionarTodasAsBandeiras(String[] bandeiras) throws InterruptedException{
		Thread.sleep(3000);
		WebElement domicilioBancario=driver.findElement(By.id("DomicilioBancario"));
		int idx=1;
		
		
		for (int i = 0; i < bandeiras.length; i++) {
			Select selectBandeira= new Select(domicilioBancario.findElement(By.name("cmbBandeira")));
			
			if(i>0){
				System.out.println("i " + idx);
				 selectBandeira= new Select(domicilioBancario.findElement(By.xpath("(//select[@id='cmbBandeira'])["+ (idx+=1)+"]")));
				 System.out.println( selectBandeira.getOptions());
			}
			System.out.println(bandeiras[i]);
			Thread.sleep(3000);
			selectBandeira.selectByVisibleText(bandeiras[i]);
			Thread.sleep(3000);
			clicarEmSalvarBandeira(domicilioBancario);
			Thread.sleep(3000);
			if(i<bandeiras.length-1){
				clicarEmAdicionarBandeira();
			}
		}

		
	}

	public void clicarEmSalvarBandeira(WebElement elem){
		List<WebElement> btnSucess= elem.findElements(By.className("btn-success"));		
		
		for (WebElement btnElement : btnSucess) {
			waitForClassName("glyphicon");
			System.out.println(btnElement.getTagName());
			//WebElement el= btnElement.findElement(By.cssSelector("button.btn.btn-success.btn-sm"));
			if(btnElement.isEnabled() && btnElement.getAttribute("title").equals("Editar")){
				System.out.println(btnElement.getAttribute("title"));
				btnElement.click();
			}
			
			
		}		
	}
	
	public void clicarEmAdicionarBandeira() throws InterruptedException{
		WebElement domicilioBancario=driver.findElement(By.id("DomicilioBancario"));
		WebElement btnAddBandeira= domicilioBancario.findElement(By.className("glyphicon-plus"));
		if(btnAddBandeira.isEnabled()){
			btnAddBandeira.click();
			Thread.sleep(2000);
		}
	}
	
	public String waitForClassName(String param) {
		WebElement elem = new WebDriverWait(driver, 20)
				.until(ExpectedConditions.visibilityOfElementLocated(By
						.className(param)));
		if (!elem.equals(param))
			return "not found";
		return param;

	}

	public void clicarNoBotaoAdicionar(String className){
		WebElement btnAdicionar= driver.findElement(By.className(className));
		btnAdicionar.click();
	}

	public void deveCadastrarOperador(String nomeOperador, boolean admin) {
		driver.findElement(By.name("txtNomeOperador")).sendKeys(nomeOperador);
		if(admin==true){
			driver.findElement(By.name("swAdministrador")).click();
		}
		WebElement gridOperador=driver.findElement(By.id("Operador"));
		gridOperador.findElement(By.className("glyphicon-floppy-disk")).click();

	}

	public void deveCadastrarCondicoesComerciais(String adquirente, String cnae) throws InterruptedException {
		Thread.sleep(1000);
		Select selectAdquirente= new Select(driver.findElement(By.name("cmbAdquirente")));
		selectAdquirente.selectByVisibleText(adquirente);
		driver.findElement(By.name("txtNameacCnae")).sendKeys(cnae);
		Thread.sleep(2000);

		WebElement divCnae=driver.findElement(By.id("listacCnae_"));
		List<WebElement> listaCnae= divCnae.findElements(By.tagName("a"));
		Thread.sleep(2000);
		for (WebElement cnaeRow : listaCnae) {
			System.out.println(cnaeRow.getText());
			if(cnaeRow.getText().equals(cnae)){
				cnaeRow.click();
			}
		}
	}

	public void avancarFase() throws InterruptedException {
		Thread.sleep(4000);
		
		new WebDriverWait(driver, 20)
		.until(ExpectedConditions.visibilityOfElementLocated(By
				.className("glyphicon-play")));
		new Actions(driver).moveToElement(driver.findElement(By.className("glyphicon-play")));

		WebElement iconeAvancarFase=driver.findElement(By.className("glyphicon-play"));
		
		iconeAvancarFase.click();
		confirmarAvancarFase();
		System.out.println(iconeAvancarFase.getText());

		//if(botaoAvancar.getText().equals("Avançar")){
//			botaoAvancar.click();
//			confirmarAvancarFase();
//		}

	}
	
	public void preenhcerDadosAreaConsultor(String area) {
		PageObjects pageObjects = new PageObjects(driver);
		pageObjects.selectOption("cmbArea", area);

	}

	public void preencherPeriodicidadeSemData(String periodicidade) {
		PageObjects pageObjects = new PageObjects(driver);
		pageObjects.selectOption("cmbPeriodicidade", periodicidade);

	}
	

	public void deveAlterarEmisteNotaFiscal() {
		driver.findElement(By.id("rtNotaFistal")).click();
	}

	public void inserirAnexos(String file) throws AWTException, InterruptedException{
		Thread.sleep(3000);
		WebElement gridResumo=driver.findElement(By.id("resumoProcessoContainer"));
		WebElement buttonUpload=gridResumo.findElement(By.name("file"));
		buttonUpload.sendKeys(file);
		Thread.sleep(5000);
		
		Thread.sleep(3000);

	}


	public void clicarEmAprovadoPelaAdquirente() throws InterruptedException {
		Thread.sleep(3000);
		driver.findElement(By.id("ckAprovadoAdquirenteTrue")).click();
	}
	
	public void clicarEmResponsavelEmpresa() throws InterruptedException {
		Thread.sleep(2000);
		driver.findElement(By.id("rdResponsavelInstalacao__Empresa")).click();
	}
	
	public void preencherMerchant(String merchant) throws InterruptedException{
		Thread.sleep(2000);
		driver.findElement(By.name("txtMerchant")).sendKeys(merchant);
	}


	public void deveCadastrarEquipamentoAdquirente(String referenceNumber, String numeroLogico) throws InterruptedException {
		Thread.sleep(3000);
		
		WebElement divEquipamentoAdquirente=driver.findElement(By.id("EquipamentoAdquirente"));
		WebElement btnAdd= divEquipamentoAdquirente.findElement(By.className("btn-success"));
		
		System.out.println(btnAdd.getText());
		btnAdd.click();
		
		Thread.sleep(2000);
		divEquipamentoAdquirente.findElement(By.name("txtNumeroReferencia")).sendKeys(referenceNumber);
		divEquipamentoAdquirente.findElement(By.name("txtNumeroLogico")).sendKeys(numeroLogico);
	
		divEquipamentoAdquirente.findElement(By.className("glyphicon-floppy-disk")).click();
	}
	
	
	public void verificarFase() throws InterruptedException {
		Thread.sleep(3000);
		boolean fase = driver.getPageSource().contains("Aguardando Consultor");

		if (fase) {
			System.out.println("GG!! By Rosinha");
		} else {
			System.out.println("Bad isso enh!! By Rosinha");
			String urlAtual= driver.getCurrentUrl();
			atualizarPagina(urlAtual);
			verificarFase();
		}
		
	}
	
	public void urlAtual(){
		String url= driver.getCurrentUrl();
		atualizarPagina(url);
	}
	
	public void atualizarPagina(String url){
		driver.get(url);
	}
	
//	public void voltarAPaginaAnterior(url) throws InterruptedException {
//		if(driver.getCurrentUrl()!=url){
//			Thread.sleep(20000);
//			driver.navigate().back();
//		}
//	}
}

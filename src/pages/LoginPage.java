package pages;

import org.junit.Test;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

import systemTest.SelectMenuSystemTest;

public class LoginPage {

	private WebDriver driver;

	public LoginPage(WebDriver driver) {
		driver = new FirefoxDriver();
		//driver.get("https://sgr-sit.redetendencia.com.br/sgr/paginas/login/login.html#/");
	}

//	@Test
//	public void logarComDadosIncorretos() {
//		WebElement user = driver.findElement(By.id("inputLogin"));
//		WebElement password = driver.findElement(By.id("inputSenha"));
//		user.sendKeys("usuario");
//		password.sendKeys("1");
//		password.submit();
//	}

	@Test
	public void logarComDadosCorretos(String usuario, String senha){
		
		WebElement user = driver.findElement(By.id("inputLogin"));
		WebElement password = driver.findElement(By.id("inputSenha"));
		System.out.printf(usuario, senha + "/n");
		user.sendKeys(usuario);
		password.sendKeys(senha);
		password.submit();
		
	}
}
